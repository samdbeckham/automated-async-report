const fs = require("fs/promises");
const { SAMS_GITLAB_ID, RETRO_COMMENTS, TEAMS } = require("./CONSTANTS");
const buildFileName = require("./helpers/buildFileName");
const buildIssueTitle = require("./helpers/buildIssueTitle");
const fetchFromGraphQl = require("./helpers/graphql");

async function fetchIssueText(team) {
  const filename = buildFileName(team);

  return fs.readFile(filename, "utf8");
}

async function createIssue(team) {
  const issueTitle = buildIssueTitle(team);
  const issueDescription = await fetchIssueText(team);

  const mutation = `
    mutation {
      createIssue(input:{
        description: """${issueDescription}""",
        title: "${issueTitle}",
        projectPath: "${team.ISSUE_PROJECT_PATH}",
        assigneeIds: ["${SAMS_GITLAB_ID}"]
        confidential: true,
      }) {
        issue {
          id
          title
          webUrl
        }
      }
    }`;
  const data = await fetchFromGraphQl(mutation, false);

  if (data && data.createIssue && data.createIssue.issue) {
    const { issue } = data.createIssue;
    console.log("Issue created succesfully");
    console.log(issue.title);
    console.log(issue.webUrl);
    return issue;
  } else {
    console.error("Failed to create issue");
    return false;
  }
}

async function addRetroComments(issue) {
  if (RETRO_COMMENTS.length === 0) {
    console.log("No retro comments to add");
    return;
  }
  return Promise.all(
    RETRO_COMMENTS.map((comment) => createNote(issue, comment))
  );
}

async function createNote(issue, comment) {
  const mutation = `
  mutation {
    createNote(
      input: {noteableId: "${issue.id}", body: """${comment}"""}
    ) {
      note {
        url
      }
    }
  }`;
  const data = await fetchFromGraphQl(mutation, false);

  if (data && data.createNote && data.createNote.note) {
    console.log("Retro comment added succesfully");
    return true;
  } else {
    console.error("Failed to add retro comment");
    return false;
  }
}

async function run() {
  for (const team of TEAMS) {
    const issue = await createIssue(team);
    if (issue && issue.id) {
      addRetroComments(issue);
    }
  }
  // Actually fail the pipeline
}

run();
