const path = require("path");
const LABEL_SEPARATOR = "::";
const WORKFLOW_PREFIX = "workflow";

const CHRIS = "chrismicek";
const JACKIE = "jackib";
const JAMES = "jrushford";
const JEFF = "jtucker_gl";
const LUKAS = "leipert";
const MARK = "markrian";
const PAUL = "pgascouvaillancourt";
const SAM = "samdbeckham";
const SCOTT = "sdejonge";
const THOMAS = "thutterer";
const VANESSA = "vanessaotto";
const MICHELLE = "m_gill";
const FLORIE = "fguibert";

const DESIGN_SYSTEM = {
  NAME: "design-system",
  LABEL: "group::design system",
  ISSUE_PROJECT_PATH: "gitlab-org/foundations/design-system/team-tasks",
  STABLE_COUNTERPARTS: {
    CHRIS,
    JEFF,
    SAM,
    MICHELLE,
  },
  TEAM_MEMBERS: {
    MARK,
    SCOTT,
    // VANESSA,
    JAMES,
    FLORIE,
  },
  OKRS: ["gid://gitlab/Issue/150835203", "gid://gitlab/Issue/150745036"],
};

const PERSONAL_PRODUCTIVITY = {
  NAME: "personal-productivity",
  LABEL: "group::personal productivity",
  ISSUE_PROJECT_PATH: "gitlab-org/foundations/personal-productivity/team-tasks",
  STABLE_COUNTERPARTS: {
    JACKIE,
    JEFF,
    SAM,
    MICHELLE,
  },
  TEAM_MEMBERS: {
    LUKAS,
    PAUL,
    THOMAS,
  },
  OKRS: ["gid://gitlab/Issue/150835203", "gid://gitlab/Issue/150746082"],
};

const CONSTANTS = {
  BREAK: "\n\n",
  DATE_FORMAT: "yyyy-MM-dd",
  HEALTH_STATUSES: {
    ON_TRACK: "onTrack",
    NEEDS_ATTENTION: "needsAttention",
    AT_RISK: "atRisk",
  },
  LABEL_SEPARATOR,
  PUBLIC_DIR: path.join(__dirname, "../public"),
  SAMS_GITLAB_ID: "gid://gitlab/User/1125848",
  WORKFLOW_LABELS: {
    PREFIX: WORKFLOW_PREFIX,
    IN_DEV: `${WORKFLOW_PREFIX + LABEL_SEPARATOR}in dev`,
    IN_REVIEW: `${WORKFLOW_PREFIX + LABEL_SEPARATOR}in review`,
    VERIFICATION: `${WORKFLOW_PREFIX + LABEL_SEPARATOR}verification`,
    CANARY: `${WORKFLOW_PREFIX + LABEL_SEPARATOR}canary`,
    STAGING: `${WORKFLOW_PREFIX + LABEL_SEPARATOR}staging`,
    PRODUCTION: `${WORKFLOW_PREFIX + LABEL_SEPARATOR}production`,
    COMPLETE: `${WORKFLOW_PREFIX + LABEL_SEPARATOR}complete`,
  },
  TEAMS: [DESIGN_SYSTEM, PERSONAL_PRODUCTIVITY],
};

module.exports = CONSTANTS;
