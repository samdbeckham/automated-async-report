const fs = require("fs");
const buildFileName = require("./helpers/buildFileName");
const generateTemplate = require("./templates/main");
const getAlmostCompletedIssues = require("./getters/almostCompletedIssues");
const getDeliverableIssues = require("./getters/deliverableIssues");
const getCompletedIssues = require("./getters/completedIssues");
const getInProgressIssues = require("./getters/inProgressIssues");
const getOkrs = require("./getters/okrs");
const getWeekData = require("./getters/week");
const { PUBLIC_DIR, TEAMS } = require("./CONSTANTS");
const getMergedMrs = require("./getters/mergedMrs");

async function fetchOptions(team) {
  const mergedMrs = await getMergedMrs(team);
  return {
    almostCompletedIssues: await getAlmostCompletedIssues(team),
    deliverableIssues: await getDeliverableIssues(team),
    completedIssues: await getCompletedIssues(team),
    inProgressIssues: await getInProgressIssues(team),
    mergedMrs,
    okrs: await getOkrs(team),
    team,
    week: getWeekData(),
  };
}

async function saveFiles(team, content) {
  const filename = buildFileName(team);
  const logError = (e) => {
    if (e) {
      console.log(e);
    }
  };

  fs.mkdir(PUBLIC_DIR, { recursive: true }, logError);
  fs.writeFile(filename, content, logError);
}

async function run() {
  for (const team of TEAMS) {
    const options = await fetchOptions(team);

    saveFiles(team, generateTemplate(options));
  }
}

run();
