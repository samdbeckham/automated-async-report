const { format, subDays } = require("date-fns");
const { DATE_FORMAT } = require("../CONSTANTS");

function getWeekData() {
  const today = new Date();

  return {
    startDate: format(subDays(today, 7), DATE_FORMAT),
    endDate: format(today, DATE_FORMAT),
    year: today.getFullYear(),
  };
}

module.exports = getWeekData;
