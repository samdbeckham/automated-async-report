const { HEALTH_STATUSES } = require("../CONSTANTS");
const formatRawIssue = require("../helpers/formatRawIssue");
const fetchFromGraphQl = require("../helpers/graphql");
const { issueFields } = require("../helpers/graphqlPartials");

async function fetchEpic({ epicIid, epicId }) {
  const query = `
    query getEpic {
      group(fullPath:"gitlab-org") {
        epic(iid:"${epicIid}") {
          title
          webUrl
          descendantWeightSum {
            openedIssues
            closedIssues
          }
        }
        issues(epicId: "${epicId}", includeSubepics:true) {
          nodes { ${issueFields} state healthStatus }
        }
      }
    }`;
  const data = await fetchFromGraphQl(query);
  return data && data.group && data.group;
}

const calculateCompletionByWeight = ({ descendantWeightSum }) => {
  if (!descendantWeightSum) {
    return 0;
  }

  const { openedIssues, closedIssues } = descendantWeightSum;

  if (openedIssues === 0 && closedIssues === 0) {
    return 0;
  }

  return parseInt((closedIssues / (openedIssues + closedIssues)) * 100);
};

async function getEpicStatus({ epicId, epicIid }) {
  const { epic, issues } = await fetchEpic({ epicId, epicIid });

  const openIssues = issues.nodes.filter((issue) => issue.state === "opened");

  return {
    ...epic,
    completionPercentageByWeight: calculateCompletionByWeight(epic),
    unweightedIssues: issues.nodes.filter((issue) => !issue.weight),
    onTrackIssues: openIssues
      .filter((issue) => issue.healthStatus === HEALTH_STATUSES.ON_TRACK)
      .map(formatRawIssue),
    needsAttentionIssues: openIssues
      .filter((issue) => issue.healthStatus === HEALTH_STATUSES.NEEDS_ATTENTION)
      .map(formatRawIssue),
    atRiskIssues: openIssues
      .filter((issue) => issue.healthStatus === HEALTH_STATUSES.AT_RISK)
      .map(formatRawIssue),
  };
}

module.exports = getEpicStatus;
