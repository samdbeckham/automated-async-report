const { WORKFLOW_LABELS } = require("../CONSTANTS");
const formatRawIssues = require("../helpers/formatRawIssues");
const fetchFromGraphQl = require("../helpers/graphql");
const { issueFields } = require("../helpers/graphqlPartials");

async function fetchIssues({ labels, assignee }) {
  const query = `
    query getIssues {
      group(fullPath:"gitlab-org") {
        issues(
          state: opened,
          assigneeUsernames:["${assignee}"],
          labelName:["${labels.join('","')}"]
          types:[ISSUE]
        ) {
          nodes { ${issueFields} }
        }
      }
    }`;
  const data = await fetchFromGraphQl(query);
  return data && data.group && data.group.issues && data.group.issues.nodes;
}

async function getAlmostCompletedIssues(team) {
  const workflowLabels = [
    WORKFLOW_LABELS.VERIFICATION,
    WORKFLOW_LABELS.CANARY,
    WORKFLOW_LABELS.STAGING,
    WORKFLOW_LABELS.PRODUCTION,
  ];
  const teamMembers = Object.values(team.TEAM_MEMBERS);
  const issues = await Promise.all(
    teamMembers.map(
      async (assignee) =>
        await Promise.all(
          workflowLabels.map(
            async (label) => await fetchIssues({ labels: [label], assignee })
          )
        )
    )
  );

  return formatRawIssues(issues);
}

module.exports = getAlmostCompletedIssues;
