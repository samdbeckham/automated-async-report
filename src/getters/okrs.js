const fetchFromGraphQl = require("../helpers/graphql");

// GraphQL Query to get these
// query getOKRIDs {
//   project(fullPath:"gitlab-com/gitlab-OKRs") {
//     issues(assigneeUsername:"samdbeckham" state:opened) {
//       nodes {
//         title
//         id
//       }
//     }
//   }
// }

const buildOKRFragment = (id, index) => `
  okr${index}:workItem(id:"${id}"){
    title
    webUrl
    widgets {
      ...on WorkItemWidgetProgress {
        type
        progress
      }
    }
  }
`;

const findProgressWidget = (widget) => widget.type === "PROGRESS";

const parseOKR = (okr) => ({
  title: okr.title,
  url: okr.webUrl,
  progress: okr.widgets.find(findProgressWidget)?.progress || 0,
});

async function fetchOKRs(team) {
  const query = `
    query getOKRs {
      ${team.OKRS.map(buildOKRFragment)}
    }`;
  const data = await fetchFromGraphQl(query);
  return data;
}

async function getOkrs(team) {
  if (!team.OKRS?.length) return null;
  const OKRs = await fetchOKRs(team);
  return Object.values(OKRs).map(parseOKR);
}

module.exports = getOkrs;
