const formatRawIssues = require("../helpers/formatRawIssues");
const fetchFromGraphQl = require("../helpers/graphql");
const { issueFields } = require("../helpers/graphqlPartials");
const getWeekData = require("../getters/week");

async function fetchIssues({ assignee, closedAfter, closedBefore }) {
  const query = `
    query getIssues {
      group(fullPath:"gitlab-org") {
        issues(
          state: closed,
          assigneeUsernames:["${assignee}"],
          closedAfter:"${closedAfter}",
          closedBefore:"${closedBefore}",
          types:[ISSUE]
        ) {
          nodes { ${issueFields} }
        }
      }
    }`;
  const data = await fetchFromGraphQl(query);
  return data && data.group && data.group.issues && data.group.issues.nodes;
}

async function getCompletedIssues(team) {
  const { startDate, endDate } = getWeekData();
  const teamMembers = Object.values(team.TEAM_MEMBERS);
  const issues = await Promise.all(
    teamMembers.map(
      async (assignee) =>
        await fetchIssues({
          closedAfter: startDate,
          closedBefore: endDate,
          assignee,
        })
    )
  );

  return formatRawIssues(issues);
}

module.exports = getCompletedIssues;
