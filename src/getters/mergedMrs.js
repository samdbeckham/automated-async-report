const formatRawMrs = require("../helpers/formatRawMrs");
const fetchFromGraphQl = require("../helpers/graphql");
const { mrFields } = require("../helpers/graphqlPartials");
const getWeekData = require("./week");

async function fetchMrs({ assignee, mergedAfter, mergedBefore }) {
  const query = `
    query getMrs {
      group(fullPath:"gitlab-org") {
        mergeRequests(
          state: merged,
          assigneeUsername: "${assignee}",
          mergedBefore: "${mergedBefore}",
          mergedAfter: "${mergedAfter}"
        ) {
          nodes { ${mrFields} }
        }
      }
    }`;
  const data = await fetchFromGraphQl(query);
  return (
    data &&
    data.group &&
    data.group.mergeRequests &&
    data.group.mergeRequests.nodes
  );
}

async function getMergedMrs(team) {
  const teamMembers = Object.values(team.TEAM_MEMBERS);
  const { startDate, endDate } = getWeekData();
  const mrs = await Promise.all(
    teamMembers.map(
      async (assignee) =>
        await fetchMrs({
          assignee,
          mergedAfter: startDate,
          mergedBefore: endDate,
        })
    )
  );

  return formatRawMrs(mrs);
}

module.exports = getMergedMrs;
