const { WORKFLOW_LABELS } = require("../CONSTANTS");
const formatRawIssues = require("../helpers/formatRawIssues");
const fetchFromGraphQl = require("../helpers/graphql");
const { issueFields } = require("../helpers/graphqlPartials");

async function fetchIssues({ labels }) {
  const query = `
    query getIssues {
      group(fullPath:"gitlab-org") {
        issues(
          state: opened,
          labelName:["${labels.join('","')}"]
          types:[ISSUE]
        ) {
          nodes { ${issueFields} }
        }
      }
    }`;
  const data = await fetchFromGraphQl(query);
  return data && data.group && data.group.issues && data.group.issues.nodes;
}

async function getDeliverableIssues(team) {
  const teamLabel = team.LABEL;
  const deliverableLabel = "Deliverable";
  const issues = await fetchIssues({ labels: [teamLabel, deliverableLabel] });

  return formatRawIssues(issues);
}

module.exports = getDeliverableIssues;
