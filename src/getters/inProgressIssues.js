const { WORKFLOW_LABELS } = require("../CONSTANTS");
const formatRawIssues = require("../helpers/formatRawIssues");
const fetchFromGraphQl = require("../helpers/graphql");
const { issueFields } = require("../helpers/graphqlPartials");

const getWorkflowRank = (label) =>
  Object.values(WORKFLOW_LABELS).indexOf(label);
const sortByWorkflowLabel = (a, b) =>
  getWorkflowRank(a.workflow) > getWorkflowRank(b.workflow) ? 1 : -1;

async function fetchIssues({ labels, assignee }) {
  const query = `
    query getIssues {
      group(fullPath:"gitlab-org") {
        issues(
          state: opened,
          assigneeUsernames:["${assignee}"],
          labelName:["${labels.join('","')}"]
          types:[ISSUE]
        ) {
          nodes { ${issueFields} }
        }
      }
    }`;
  const data = await fetchFromGraphQl(query);
  return data && data.group && data.group.issues && data.group.issues.nodes;
}

async function getInProgressIssues(team) {
  const workflowLabels = [WORKFLOW_LABELS.IN_DEV, WORKFLOW_LABELS.IN_REVIEW];
  const teamMembers = Object.values(team.TEAM_MEMBERS);
  const issues = await Promise.all(
    teamMembers.map(
      async (assignee) =>
        await Promise.all(
          workflowLabels.map(
            async (label) => await fetchIssues({ labels: [label], assignee })
          )
        )
    )
  );

  return formatRawIssues(issues).sort(sortByWorkflowLabel);
}

module.exports = getInProgressIssues;
