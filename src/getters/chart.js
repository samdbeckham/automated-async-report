const fetchFromGraphQl = require("../helpers/graphql");

async function getIssueIds() {
  const query = `
  query pastIssues {
    project(fullPath:"gitlab-org/manage/foundations/team-tasks") {
      issues(labelName:"async-update" sort:CREATED_DESC first:7) {
        nodes {
          id
        }
      }
    }
  }`;
  const data = await fetchFromGraphQl(query);
  return (
    data &&
    data.project &&
    data.project.issues &&
    data.project.issues.nodes
  ).map((node) => node.id);
}

async function getRelatedMrCountForIssue(id) {
  const query = `
  query relatedMrCount {
    issue(id:"${id}") {
      relatedMergeRequests {
        count
      }
    }
  }`;
  const data = await fetchFromGraphQl(query);
  return (
    data &&
    data.issue &&
    data.issue.relatedMergeRequests &&
    data.issue.relatedMergeRequests.count
  );
}

async function getChartUrl({ mergedMrs }) {
  const apiEndpoint = "https://image-charts.com/chart.js/2.8.0";
  const issueIds = await getIssueIds();
  const mrCounts = await Promise.all(
    issueIds.map(async (id) => await getRelatedMrCountForIssue(id))
  );
  const filteredMrCounts = mrCounts.filter((count) => count > 1).reverse();
  const currentMrCount = mergedMrs.length;

  filteredMrCounts.push(currentMrCount);

  const c = {
    type: "line",
    data: {
      datasets: [
        {
          data: filteredMrCounts,
          label: "MRs",
          borderColor: "#617ae2",
          fill: false,
          borderWidth: 2,
        },
      ],
      labels: filteredMrCounts,
    },
    options: {
      legend: {
        display: false,
      },
      scales: {
        x: {
          display: false,
        },
      },
    },
  };

  const parsedC = encodeURI(JSON.stringify(c)).replace("#", "%23");
  return `${apiEndpoint}?encoding=url&c=${parsedC}&height=150&width=500`;
}

async function getCharts({ mergedMrs }) {
  // We only have one for now, lets hardcode it
  return [
    {
      title: "MR Sparkline",
      description:
        "The trend of the the merged MRs in the past few weeks. We're not tracking numbers here, just general trends.",
      url: await getChartUrl({ mergedMrs }),
    },
  ];
}

module.exports = getCharts;
