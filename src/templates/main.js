const { BREAK } = require("../CONSTANTS");
const generateLabels = require("./labels");
const generateMilestoneTracking = require("./milestone_tracking");
const generateOKRs = require("./okrs");

function generateTemplate({
  almostCompletedIssues,
  completedIssues,
  deliverableIssues,
  inProgressIssues,
  mergedMrs,
  okrs,
  team,
}) {
  let template = "";

  if (
    completedIssues?.length ||
    almostCompletedIssues?.length ||
    inProgressIssues?.length ||
    mergedMrs?.length ||
    deliverableIssues?.length
  ) {
    template += BREAK;
    template += generateMilestoneTracking({
      almostCompletedIssues,
      completedIssues,
      deliverableIssues,
      inProgressIssues,
      mergedMrs,
      team,
    });
  }

  if (okrs) {
    template += BREAK;
    template += generateOKRs({ okrs });
  }

  template += BREAK;
  template += generateLabels(team);

  return template;
}

module.exports = generateTemplate;
