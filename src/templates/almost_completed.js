const { BREAK } = require("../CONSTANTS");
const generateIssue = require("./issue");

function generateAlmostCompleted({ almostCompletedIssues }) {
  let template = `### Almost Completed`;

  template += BREAK;
  template += "Issues that are complete, but not yet verified on production";
  template += BREAK;
  template += almostCompletedIssues.map(generateIssue).join("\n");

  return template;
}

module.exports = generateAlmostCompleted;
