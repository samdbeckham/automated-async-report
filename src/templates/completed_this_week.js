const { BREAK } = require("../CONSTANTS");
const generateIssue = require("./issue");
const generateMr = require("./mr");

function generateCompletedThisWeek({ completedIssues, mergedMrs }) {
  let template = `### Completed this week`;

  if (completedIssues?.length) {
    template += BREAK;
    template += `#### Issues`;
    template += BREAK;
    template += completedIssues.map(generateIssue).join("\n");
  }

  if (mergedMrs?.length) {
    template += BREAK;
    template += `#### MRs`;
    template += BREAK;
    template += mergedMrs.map(generateMr).join("\n");
  }

  return template;
}

module.exports = generateCompletedThisWeek;
