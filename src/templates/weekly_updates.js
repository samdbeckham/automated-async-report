const { BREAK } = require("../CONSTANTS");
const generateHiring = require("./hiring");
const generateIncidents = require("./incidents");
const generateOKRs = require("./okrs");

function generateWeeklyUpdates({ incidents, okrs, openReqs }) {
  let template = `## Weekly Updates`;

  if (okrs) {
    template += BREAK;
    template += generateOKRs({ okrs });
  }

  if (openReqs) {
    template += BREAK;
    template += generateHiring({ openReqs });
  }

  if (incidents) {
    template += BREAK;
    template += generateIncidents({ incidents });
  }

  return template;
}

module.exports = generateWeeklyUpdates;
