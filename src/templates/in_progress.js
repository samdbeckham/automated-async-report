const { BREAK } = require("../CONSTANTS");
const generateIssue = require("./issue");

function generateInProgress({ inProgressIssues }) {
  let template = `### In Progress`;

  template += BREAK;
  template += inProgressIssues.map(generateIssue).join("\n");

  return template;
}

module.exports = generateInProgress;
