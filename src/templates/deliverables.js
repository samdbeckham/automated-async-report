const { BREAK } = require("../CONSTANTS");
const generateIssue = require("./issue");

function generateDeliverableIssues({ deliverableIssues }) {
  let template = `### Deliverables`;

  template += BREAK;
  template += "Issues we absolutely must ship this milestone";
  template += BREAK;
  template += deliverableIssues.map(generateIssue).join("\n");

  return template;
}

module.exports = generateDeliverableIssues;
