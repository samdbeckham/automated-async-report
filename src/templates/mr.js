function generateMr({ url }) {
  return `- ${url}+s`;
}

module.exports = generateMr;
