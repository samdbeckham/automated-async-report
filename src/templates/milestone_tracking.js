const { BREAK } = require("../CONSTANTS");
const prettyTeamName = require("../helpers/prettyTeamName");
const generateAlmostCompleted = require("./almost_completed");
const generateCompletedThisWeek = require("./completed_this_week");
const generateDeliverableIssues = require("./deliverables");
const generateInProgress = require("./in_progress");

function generateMilestoneTracking({
  almostCompletedIssues,
  completedIssues,
  deliverableIssues,
  inProgressIssues,
  mergedMrs,
  team,
}) {
  const teamName = prettyTeamName(team);
  let template = `## Milestone Tracking: ${teamName}`;

  if (deliverableIssues?.length) {
    template += BREAK;
    template += generateDeliverableIssues({ deliverableIssues });
  }

  if (completedIssues?.length || mergedMrs?.length) {
    template += BREAK;
    template += generateCompletedThisWeek({ completedIssues, mergedMrs });
  }

  if (almostCompletedIssues?.length) {
    template += BREAK;
    template += generateAlmostCompleted({ almostCompletedIssues });
  }

  if (inProgressIssues?.length) {
    template += BREAK;
    template += generateInProgress({ inProgressIssues });
  }

  return template;
}

module.exports = generateMilestoneTracking;
