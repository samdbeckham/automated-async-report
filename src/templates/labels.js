const { BREAK } = require("../CONSTANTS");

function generateCCLine(team) {
  let line = "cc: ";
  const addCC = (username) => {
    line += `@${username} `;
  };

  Object.values(team.STABLE_COUNTERPARTS).forEach(addCC);
  Object.values(team.TEAM_MEMBERS).forEach(addCC);

  return line;
}

function generateLabels(team) {
  let template = "";

  template += generateCCLine(team);
  template += BREAK;
  template +=
    "If any of y'all prefer to stop being notified here, let me know and I'll remove you from the template.";
  template += BREAK;
  template += "/label ~async-update";
  template += "\n";
  template += "/assign @samdbeckham";
  return template;
}

module.exports = generateLabels;
