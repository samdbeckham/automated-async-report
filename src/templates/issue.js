function generateIssue({ healthStatus, type, url, workflow }) {
  let string = `- ${url}+s`;
  if (type) {
    string += ` ~"${type}"`;
  }
  if (workflow) {
    string += ` ~"${workflow}"`;
  }
  if (healthStatus) {
    switch (healthStatus) {
      case "onTrack":
        return (string += " `🟢 On Track`");
      case "needsAttention":
        return (string += " `🟡 Needs Attention`");
      case "atRisk":
        return (string += " `🔴 At Risk`");
      default:
        return "";
    }
  }
  return string;
}

module.exports = generateIssue;
