const { BREAK } = require("../CONSTANTS");

function generateTrendingChart({ charts }) {
  let template = `## Trending Charts`;

  charts.forEach((chart) => {
    template += BREAK;
    template += `### ${chart.title}`;
    template += BREAK;
    template += chart.description;
    template += BREAK;
    template += `![](${chart.url})`;
  });

  return template;
}

module.exports = generateTrendingChart;
