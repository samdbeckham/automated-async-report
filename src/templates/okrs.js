const { BREAK } = require("../CONSTANTS");

function generateOKRs({ okrs }) {
  let template = `## OKRs`;

  template += BREAK;
  template += okrs.map(generateOKR).join(BREAK);

  return template;
}

function generateOKR({ title, url, progress }) {
  let template = `### [${title}](${url}) ${progress}%`;
  return template;
}

module.exports = generateOKRs;
