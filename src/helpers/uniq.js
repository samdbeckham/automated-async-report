const uniq = (array, key) => {
  if (!array?.length) {
    return [];
  }

  const cleanArray = Array.from(new Set(array.map((i) => i[key])));

  if (array.length === cleanArray.length) {
    return array;
  }

  return cleanArray.map((i) => array.find((j) => j[key] === i));
};

module.exports = uniq;
