const getWeekData = require("../getters/week");
const prettyTeamName = require("./prettyTeamName");

function buildIssueTitle(team) {
  const { startDate, endDate } = getWeekData();
  const teamName = prettyTeamName(team);
  return `${teamName}: Engineering Async Update (${startDate} – ${endDate})`;
}

module.exports = buildIssueTitle;
