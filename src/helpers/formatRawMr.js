const formatRawMr = (mr) => ({
  url: mr.webUrl,
});

module.exports = formatRawMr;
