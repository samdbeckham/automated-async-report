const fetch = require("make-fetch-happen");
const { GITLAB_TOKEN } = process.env;

async function fetchFromGraphQl(rawQuery, replaceWhiteSpace = true) {
  const query = replaceWhiteSpace ? rawQuery.replace(/\n/g, "") : rawQuery;
  const request = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${GITLAB_TOKEN}`,
    },
    body: JSON.stringify({ query }),
  };
  const response = await fetch("https://gitlab.com/api/graphql", request);
  const { data, errors } = await response.json();
  if (errors) {
    console.warn(errors);
  }
  return data;
}

module.exports = fetchFromGraphQl;
