const formatRawIssue = require("./formatRawIssue");
const uniq = require("./uniq");

const formatRawIssues = (issues) =>
  uniq(issues.flat(10).map(formatRawIssue), "url");

module.exports = formatRawIssues;
