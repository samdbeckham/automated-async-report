const getLabel = (labels, regex) =>
  labels.find(({ title }) => title.match(regex))?.title;
const getTypeLabel = (labels) => getLabel(labels, /^type::.*/);
const getWorkflowLabel = (labels) => getLabel(labels, /^workflow::.*/);

const formatRawIssue = ({ healthStatus, labels, webUrl }) => ({
  healthStatus,
  type: getTypeLabel(labels.nodes),
  url: webUrl,
  workflow: getWorkflowLabel(labels.nodes),
});

module.exports = formatRawIssue;
