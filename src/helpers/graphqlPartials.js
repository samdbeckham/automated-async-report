const labels = `labels { nodes { title } }`;
const firstAssignee = `assignees(first:1) { nodes { username } }`;
const issueFields = `${firstAssignee} ${labels} webUrl healthStatus`;
const mrFields = `webUrl`;

module.exports = {
  issueFields,
  mrFields,
};
