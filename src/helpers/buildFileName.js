const { PUBLIC_DIR } = require("../CONSTANTS");
const getWeekData = require("../getters/week");

function buildFileName(team) {
  const { endDate } = getWeekData();
  return `${PUBLIC_DIR}/${team.NAME}_${endDate}.md`;
}

module.exports = buildFileName;
