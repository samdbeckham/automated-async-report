function prettyTeamName(team) {
  return team.NAME.split("-")
    .map((word) => word[0].toUpperCase() + word.slice(1))
    .join(" ");
}

module.exports = prettyTeamName;
