const formatRawMr = require("./formatRawMr");
const uniq = require("./uniq");

const formatRawMrs = (mrs) => uniq(mrs.flat(10).map(formatRawMr), "url");

module.exports = formatRawMrs;
