const fs = require("fs/promises");
const { TEAMS } = require("./CONSTANTS");
const fetchFromGraphQl = require("./helpers/graphql");

async function getOldIssues(team) {
  const query = `
    query {
      project(fullPath: "${team.ISSUE_PROJECT_PATH}") {
        issues(labelName: ["async-update"], state: opened) {
          nodes {
            iid
          }
        }
      }
    }
  `;
  const data = await fetchFromGraphQl(query, true);
  return data?.project?.issues?.nodes;
}

async function closeIssue(issue, team) {
  const mutation = `
    mutation {
      updateIssue(input: {
        projectPath: "${team.ISSUE_PROJECT_PATH}",
        iid: "${issue.iid}",
        stateEvent: CLOSE
      }) {
        issue {
          title
        }
      }
    }
  `;
  const data = await fetchFromGraphQl(mutation, true);
  console.log(`closed: ${data?.updateIssue?.issue?.title}`);
}

async function run() {
  for (const team of TEAMS) {
    const issues = await getOldIssues(team);
    for (const issue of issues) {
      await closeIssue(issue, team);
    }
  }
}

run();
